import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';


import { TitleTagService } from 'src/app/service/seo/title-tag-service.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.less']
})
export class BlogComponent implements OnInit {

  constructor(
    // private meta: Meta, private title: Title,

    private titleTagService: TitleTagService
  ) {
    // this.meta.addTags([
    //   { name: 'description', content: 'Blog page of SEO friendly app' },
    //   { name: 'author', content: 'Blog buttercms' },
    //   { name: 'keywords', content: 'Blog Angular, ButterCMS' }
    // ]);
    // this.setTitle('Blog Page');



  }
  // public setTitle(newTitle: string) {
  //   this.title.setTitle(newTitle);
  // }
  ngOnInit() {


    /* Meta Service */
    this.titleTagService.setTitle('Blog Page 123');
    this.titleTagService.setSocialMediaTags(
      "http://leo9studio.in/projects/ng-investorial/article/responsible-investment",
      'Blog Page',
      'Blog page of SEO friendly app',
      'https://nbadmin.investorial.com/storage/articles/June2020/060820201518375ede56cd8e9b6.jpg'
    );
    /* // Meta Service */

  }
}