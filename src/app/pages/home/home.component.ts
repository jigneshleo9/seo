import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { TitleTagService } from 'src/app/service/seo/title-tag-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  constructor(
    // private meta: Meta, private title: Title,

    private titleTagService: TitleTagService
  ) {
    // this.meta.addTags([
    //   { name: 'description', content: 'Home page of SEO friendly app' },
    //   { name: 'author', content: 'buttercms' },
    //   { name: 'keywords', content: 'Angular, ButterCMS' }
    // ]);
    // this.setTitle('Home Page');
  }
  // public setTitle(newTitle: string) {
  //   this.title.setTitle(newTitle);
  // }
  ngOnInit() {


    /* Meta Service */
    this.titleTagService.setTitle('Home Page 123');
    this.titleTagService.setSocialMediaTags(
      "http://leo9studio.in/projects/ng-investorial/article/responsible-investment",
      'Home Page',
      'Home page of SEO friendly app',
      'https://nbadmin.investorial.com/storage/articles/June2020/060820201519255ede56fd91547.jpg'
    );
    /* // Meta Service */

  }
}